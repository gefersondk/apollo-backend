# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Home',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('titulo_banner', models.CharField(max_length=100)),
                ('subtitulo_banner', models.TextField(max_length=500)),
                ('texto_de_apresentacao', models.TextField(max_length=500)),
                ('texto_valores', models.TextField(max_length=500)),
                ('texto_fundadores', models.TextField(max_length=500)),
                ('texto_plano_de_fidelidade', models.TextField(max_length=500)),
                ('texto_como_contratar', models.TextField(max_length=500)),
                ('texto_formulario_contato_comum', models.TextField(max_length=500)),
                ('texto_formulario_contato_orcamento', models.TextField(max_length=500)),
            ],
            options={
                'verbose_name': 'Home',
                'verbose_name_plural': 'Home',
            },
        ),
        migrations.CreateModel(
            name='Rodape',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('texto_rodape1', models.TextField(max_length=500)),
                ('texto_rodape2', models.TextField(max_length=500)),
            ],
            options={
                'verbose_name': 'Rodape',
                'verbose_name_plural': 'Rodape',
            },
        ),
        migrations.CreateModel(
            name='Servico',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('titulo', models.CharField(max_length=100)),
                ('descricao', models.TextField(max_length=500)),
            ],
            options={
                'verbose_name': 'Servi\xe7o',
                'verbose_name_plural': 'Servi\xe7o',
            },
        ),
    ]
