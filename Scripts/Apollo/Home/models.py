# -*- coding: utf-8 -*-
from django.db import models

# Create your models here.

class Servico(models.Model):
    titulo = models.CharField(max_length=100)
    descricao = models.TextField(max_length=500)
    class Meta:
        verbose_name = 'Serviço'
        verbose_name_plural = 'Serviço'
    #imagem
    def __str__(self):
        return self.titulo

class Home(models.Model):
    titulo_banner = models.CharField(max_length=100)
    subtitulo_banner = models.TextField(max_length=500)
    texto_de_apresentacao = models.TextField(max_length=500)
    texto_valores = models.TextField(max_length=500)
    texto_fundadores = models.TextField(max_length=500)
    texto_plano_de_fidelidade = models.TextField(max_length=500)
    #youtube_video_explicativo = models.CharField(max_length=100)
    texto_como_contratar = models.TextField(max_length=500)
    texto_formulario_contato_comum = models.TextField(max_length=500)
    texto_formulario_contato_orcamento = models.TextField(max_length=500)
    #servicos = models.ManyToManyField(Servico, verbose_name="Lista de Serviços")
    class Meta:
        verbose_name = 'Home'
        verbose_name_plural = 'Home'


    def __str__(self):
        return 'Dados Home'


class Rodape(models.Model):
    texto_rodape1 = models.TextField(max_length=500)
    texto_rodape2 = models.TextField(max_length=500)
    class Meta:
        verbose_name = 'Rodape'
        verbose_name_plural = 'Rodape'
    def __str__(self):
        return 'Dados Rodape'
    #imagem
        #db_table = 'music_album'