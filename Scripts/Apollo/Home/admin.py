from django.contrib import admin
from  models import Home
from models import Servico
from models import Rodape
# Register your models here.


class HomeAdmin(admin.ModelAdmin):
  def has_add_permission(self, request):
    num_objects = self.model.objects.count()
    if num_objects >= 1:
      return False
    else:
      return True


admin.site.register(Home, HomeAdmin)
admin.site.register(Servico)
admin.site.register(Rodape, HomeAdmin)